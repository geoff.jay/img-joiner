[GtkTemplate (ui="/org/gnome/ImgJoiner/window.ui")]
public class ImgJoinerWindow : Gtk.ApplicationWindow {

    private Gst.Pipeline pipeline;
    private Gst.Element source;
    private Gst.Element sink;
    private int start_index = 280;
    private string input_location = "IMG_%04d.JPG";
    private string output_location = "video.ogg";
    // private string caps = "image/jpeg,framerate=(fraction)30/1,pixel-aspect-ratio=1/1";

    [GtkChild]
    private Gtk.FileChooserButton btn_folder;

    [GtkChild]
    private Gtk.Entry entry_file;

    public ImgJoinerWindow (Gtk.Application application) {
        Object (application: application);

        /* Add the actions */
        var combine_action = new SimpleAction ("combine", null);
        combine_action.activate.connect (combine_activated_cb);
        application.add_action (combine_action);

        entry_file.text = output_location;

        /* Connect callbacks */
        btn_folder.selection_changed.connect (selection_changed_cb);

        init_gst ();
    }

    private void init_gst () {
        string launch_str = "multifilesrc name=source ! jpegdec ! videorate ! videoconvert ! theoraenc ! oggmux ! filesink name=sink";

        try {
            pipeline = (Gst.Pipeline) Gst.parse_launch (launch_str);
        } catch (Error e) {
            critical (e.message);
        }

        source = pipeline.get_by_name ("source");
        sink = pipeline.get_by_name ("sink");

        /* Set the input properties */
        debug ("Setting source element properties");
        debug (" > location: %s", input_location);
        source.set ("location", input_location);
        debug (" > start-index: %d", start_index);
        source.set ("start-index", start_index);
        // debug (" > caps: %s", caps);
        // source.set ("caps", caps);

        /* Set the output properties */
        debug ("Setting sink element properties");
        debug (" > location: %s", output_location);
        sink.set ("location", output_location);

        pipeline.set_state (Gst.State.PAUSED);
    }

    private void display_error (string message) {
        var dialog = new Gtk.MessageDialog (this,
                                            Gtk.DialogFlags.MODAL,
                                            Gtk.MessageType.ERROR,
                                            Gtk.ButtonsType.OK,
                                            message);
        dialog.response.connect ((response_id) => {
            debug ("Error acknowledged");
            dialog.destroy ();
            return;
        });
        dialog.show ();
    }

    private void selection_changed_cb () {
        debug ("Path %s selected", btn_folder.get_current_folder ());

        // string temp = input_location;
        // input_location = btn_folder.get_current_folder () + "/" + temp;
        // source.set ("location", input_location);

        // debug ("Source location: %s", input_location);

        Posix.chdir (btn_folder.get_current_folder ());
    }

    /**
     * Perform equivalent of the command:
     *
     *   gst-launch-1.0 multifilesrc location="IMG_%04d.JPG" start-index=280 \
     *     caps="image/jpeg,framerate=(fraction)30/1,pixel-aspect-ratio=1/1" ! \
     *     jpegdec ! videorate ! videoconvert ! theoraenc ! oggmux ! \
     *     filesink location="output.ogg"
     */
    private void combine_activated_cb (SimpleAction action, Variant? param) {
        debug ("Combine files");

        // output_location = btn_folder.get_current_folder () + "/" + entry_file.text;
        // sink.set ("location", output_location);
        // debug ("Sink location: %s", output_location);

        // Value inloc = Value (typeof (string));
        // Value outloc = Value (typeof (string));

        // source.get_property ("location", ref inloc);
        // sink.get_property ("location", ref outloc);

        // debug ("in: %s", inloc.get_string ());
        // debug ("out: %s", outloc.get_string ());

        /* Run the pipeline */
        debug ("Setting the pipeline state to PLAYING");
        var ret = pipeline.set_state (Gst.State.PLAYING);
        if (ret == Gst.StateChangeReturn.FAILURE) {
            display_error ("Unable to play the pipeline");
            return;
        }

        /* Wait until an error or EOS is received */
        debug ("Waiting for the pipeline to execute");
        var bus = pipeline.get_bus ();
        var msg = bus.timed_pop_filtered (Gst.CLOCK_TIME_NONE,
                                          Gst.MessageType.ERROR
                                          | Gst.MessageType.EOS);

        /* Parse the message */
        if (msg != null) {
            switch (msg.type) {
                case Gst.MessageType.ERROR:
                    Error err;
                    string info;

                    msg.parse_error (out err, out info);
                    string str = "Received error from %s\n%s".printf (
                                    msg.src.name, err.message);
                    display_error (str);
                    debug ("Information: %s", (info != null) ? info : "none");
                    break;
                case Gst.MessageType.EOS:
                    debug ("Received EOS message.");
                    break;
                default:
                    assert_not_reached ();
            }
        }

        /* Free resources */
        debug ("Setting the pipeline state to NULL");
        pipeline.set_state (Gst.State.NULL);
    }
}
