/* main.vala
 *
 * Copyright (C) 2017 Geoff Johnson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

public class ImgJoiner : Gtk.Application {

    internal ImgJoiner () {
        Object (application_id: "org.gnome.ImgJoiner",
                flags: ApplicationFlags.FLAGS_NONE);
    }

    protected override void activate () {
        var window = new ImgJoinerWindow (this);
        window.set_default_size (200, 140);

        /* Do any Gtk setup including adding widgets */
        Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = true;

        window.show_all ();
    }

    public static int main (string[] args) {
        Gtk.init (ref args);
        Gst.init (ref args);

        var app = new ImgJoiner ();
        int ret = app.run (args);

        return ret;
    }
}
